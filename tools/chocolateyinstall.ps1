﻿$ErrorActionPreference = 'Stop';
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$packageName = 'logitech-g-hub'
$fileType = 'exe'
$url = 'https://download01.logi.com/web/ftp/pub/techsupport/gaming/lghub_installer.exe'
$silentArgs = '/S'

Install-ChocolateyPackage $packageName $fileType $silentArgs $url